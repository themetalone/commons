package de.themetalone.commons.exceptions.database;

/**
 * Created by Steffen on 20.10.2015.
 */
public class NoDatabaseSetException extends GenericDatabaseException {

    public NoDatabaseSetException(){}
    public NoDatabaseSetException(String message){
        super(message);
    }
}
