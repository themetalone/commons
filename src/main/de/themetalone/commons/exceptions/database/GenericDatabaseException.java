package de.themetalone.commons.exceptions.database;

import de.themetalone.commons.exceptions.TheMetalOneCommonsException;

/**
 * Created by Steffen on 15.10.2015.
 */
public class GenericDatabaseException extends TheMetalOneCommonsException {

    public GenericDatabaseException(){}
    public GenericDatabaseException(String message){
        super(message);
    }

}
