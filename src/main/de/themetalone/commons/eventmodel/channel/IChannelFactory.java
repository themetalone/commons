package de.themetalone.commons.eventmodel.channel;


import de.themetalone.commons.eventmodel.channel.Channel;

/**
 * Created by themetalone on 10.07.15.
 */
public interface IChannelFactory {

    /**
     * Returns a channel for that name
     * @param name of the Channel
     * @return Channel
     */
    Channel getChannelForName(String name);

}
