package de.themetalone.commons.database.sql;

import de.themetalone.commons.database.DatabaseConnector;
import de.themetalone.commons.exceptions.database.GenericDatabaseException;
import de.themetalone.commons.exceptions.database.NoDatabaseSetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * A database connector for sqlite databases. Queues commands to reduce connection time with the databse
 * Created by Steffen on 15.10.2015.
 */
public class QueuedSqliteDatabaseConnector implements DatabaseConnector {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(QueuedSqliteDatabaseConnector.class);

    private LinkedBlockingQueue<String> queue;

    private String connectionString;

    private Connection connection;

    private int queueLimit = 10;

    private long timerTime = 60000;

    private Timer timer;

    private TimerTask timerTask;

    public QueuedSqliteDatabaseConnector(int limit){
        queueLimit = limit;
        timer = new Timer();
        queue = new LinkedBlockingQueue<>();
    }
    public QueuedSqliteDatabaseConnector(){
        timer = new Timer();
        queue = new LinkedBlockingQueue<>();
    }


    /**
     *
     * @param command a SQL command without a result as {@link String}
     * @throws GenericDatabaseException
     */
    public void executeCommand(String command) throws GenericDatabaseException {
        initConnection();
        try {
            Statement s = connection.createStatement();
            s.execute(command);
        } catch (SQLException e) {
            String message = "Could not execute command \""+command+"\" since "+e.getClass().getName()+":"+e.getMessage();
            LOG.error(message);
            throw new GenericDatabaseException(message);
        }

    }

    public List<Object> query(String query) throws GenericDatabaseException {
        try {
            List<Object> resultList = new LinkedList<>();
            initConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            ResultSetMetaData meta = resultSet.getMetaData();
            int columnCount = meta.getColumnCount();
            while (resultSet.next()){

                Map<String, String> resultMap = new HashMap<>();
                for(int i=0;i<columnCount;i++){
                    resultMap.put(meta.getColumnLabel(i),resultSet.getString(i));
                }
                resultList.add(resultMap);
            }
            return resultList;
        } catch (SQLException e) {
            String message = "Could not \""+query+"\" database because "+e.getClass().getName()+":"+e.getMessage();
            LOG.error(message);
            throw new GenericDatabaseException(message);
        }
    }


    public synchronized List<Map<String, String>> getEntries(Map<String, String> idAttributeValueMap, String[] entryAttributes, String location) throws GenericDatabaseException {
        String selectStatement = "SELECT * FROM "+location+" WHERE ";
        int idCounter = 0;
        for(String key : idAttributeValueMap.keySet()){
            idCounter++;
            selectStatement += key + " = '" + idAttributeValueMap.get(key)+"'";
            if(idCounter<idAttributeValueMap.size()){
                selectStatement+=" AND ";
            }
        }
        selectStatement+=";";
        try {
            List<Map<String, String>> resultList = new LinkedList<>();
            initConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectStatement);
            while (resultSet.next()){
                Map<String, String> resultMap = new HashMap<>();
                for(String key : entryAttributes){
                    resultMap.put(key, resultSet.getString(key));
                }
                resultList.add(resultMap);
            }
            return resultList;
        } catch (SQLException e) {
            String message = "Could not query database because "+e.getClass().getName()+":"+e.getMessage();
            LOG.error(message);
            throw new GenericDatabaseException(message);
        }
    }


    public synchronized void setEntry(Map<String, String> attributeValueMap, String location) throws GenericDatabaseException {
        String columnString = "(";
        String valueString="";
        for(String key : attributeValueMap.keySet()){
            columnString += key+",";
            valueString += attributeValueMap.get(key)+",";
        }
        // should remove the last ','
        columnString = columnString.substring(0,columnString.length()-2)+")";
        valueString = valueString.substring(0,valueString.length()-2);
        String commitMessage = "REPLACE INTO "+location+columnString+" VALUES "+valueString;
        queue.add(commitMessage);
        if(queue.size() == queueLimit) commit();
    }

    /**
     * @see DatabaseConnector#setDatabase(Object, Object)
     * @param database {@link String} path to the database
     * @param credentials are currently ignored
     * @throws GenericDatabaseException
     */
    public void setDatabase(Object database, Object credentials) throws GenericDatabaseException {
        String databaseLocation;

        if(database instanceof String){
            databaseLocation = (String) database;
        }else{
            String message="Invalid input for database: "+database.getClass().getName()+" instead of String";
            LOG.error(message);
            throw new GenericDatabaseException(message);
        }
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            String message = "Could not load database driver:"+e.getMessage();
            LOG.error(message);
            throw new GenericDatabaseException(message);
        }
        String oldConnectionString = connectionString;
        connectionString = "jdbc:sqlite:"+databaseLocation;
        try {
            Connection c = DriverManager.getConnection(connectionString);
            try{
                c.close();
            }catch (SQLException e){
                LOG.warn(e.getClass().getName()+":"+e.getMessage());
            }
        } catch (SQLException e) {
            connectionString = oldConnectionString;
            String message = "Could not connect to "+databaseLocation+" because "+e.getClass().getName()+":"+e.getMessage();
            LOG.error(message);
            throw new GenericDatabaseException(message);
        }


    }

    public void close() {
        if(!queue.isEmpty()){
            try {
                commit();
            } catch (NoDatabaseSetException e) {
                LOG.warn("Can't close what's not there");
            }
        }
        try {
            if(!connection.isClosed()){
                connection.close();
            }
        } catch (SQLException e) {
            LOG.warn("Some Error occurred while closing the connection to the database. "+e.getClass().getName()+":"
            +e.getMessage());
        }
        if(timer != null){
            timer.cancel();
        }
    }

    private synchronized void commit() throws NoDatabaseSetException {
        initConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            LOG.error("Could not commit changes to the database because "+e.getClass().getName()+":"+e.getMessage());
            return;
        }
        while (queue.peek()!=null){
            String commitStatement = queue.poll();
            try {
                statement.execute(commitStatement);
            } catch (SQLException e) {
                LOG.warn("Could not commit \""+connectionString+":"+commitStatement+"\" because "
                + e.getClass().getName()+":"+e.getMessage());
            }
        }
    }

    private synchronized void initConnection() throws NoDatabaseSetException {
        if(connection == null){
            String exceptionMessage = "Could not connect to a database since there is none set. Please set one first";
            LOG.error(exceptionMessage);
            throw new NoDatabaseSetException(exceptionMessage);
        }
        try {
            if(connection.isClosed()){
                connection = DriverManager.getConnection(connectionString);
                timer = new Timer();
                timerTask = new CloseConnectionTask(connection);
                timer.schedule(timerTask,timerTime);
            }else{
                timerTask.cancel();
                timerTask = new CloseConnectionTask(connection);
                timer.purge();
                timer.schedule(timerTask,timerTime);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    class CloseConnectionTask extends TimerTask{

        private Logger LOG = LoggerFactory.getLogger(CloseConnectionTask.class);

        private Connection connection;

        public CloseConnectionTask(Connection connection){
            super();
            this.connection = connection;
        }

        @Override
        public void run() {
            try {
                if(!connection.isClosed()){
                    connection.close();
                }
            } catch (SQLException e) {
                LOG.warn("Could not close the connection because "+e.getClass().getName()+":"+e.getMessage());
            }
        }
    }

}
