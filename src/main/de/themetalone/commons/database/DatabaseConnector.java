package de.themetalone.commons.database;

import de.themetalone.commons.exceptions.database.GenericDatabaseException;

import java.util.List;
import java.util.Map;

/**
 * Generic interface to connect to a database
 * Created by Steffen on 15.10.2015.
 */
public interface DatabaseConnector {

    /**
     * executes a no-return-value command on the database
     * @param command the command as a {@link String}
     * @throws GenericDatabaseException when something goes wrong
     */
    void executeCommand(String command) throws GenericDatabaseException;

    /**
     * Executes a query on the databse
     * @param query as {@link String}
     * @return {@link List}&lt;{@linkObject}> not null. Each entry in the list represents an result object of the query
     * @throws GenericDatabaseException
     */
    List<Object> query(String query) throws GenericDatabaseException;

    /**
     * Returns entries from the database
     * @param idAttributeValueMap a map containing key attributes with their values of the form &lt;attributeName, attributeValue>
     * @param entryAttributes a String array containing the attributes names for this entry
     * @param location the location in this the entry is stored. In most databases this corresponds to the table name
     * @return {@link List}&lt;{@link Map}&lt<attributeName,attributeValue>> not null. Each map represents an entry
     * @throws GenericDatabaseException when something goes wrong
     */
    List<Map<String, String>> getEntries(Map<String, String> idAttributeValueMap, String[] entryAttributes, String location) throws GenericDatabaseException;

    /**
     * Sets an entry
     * @param attributeValueMap the map containing the entries attributes names and values of the form &lt;attributeName, attributeValue>
     * @param location the location in this the entry is stored. In most databases this corresponds to the table name
     * @throws GenericDatabaseException when something goes wrong
     */
    void setEntry(Map<String,String> attributeValueMap, String location) throws GenericDatabaseException;

    /**
     * Sets the database to be used
     * @param database an object referencing the databse
     * @param credentials credentials of the database
     * @throws GenericDatabaseException
     */
    void setDatabase(Object database, Object credentials) throws GenericDatabaseException;

    /**
     * Closes any open connection to the database
     */
    void close();


}
