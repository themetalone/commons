package de.themetalone.commons.eventmodel.event.handler;

import de.themetalone.commons.eventmodel.event.Event;

/**
 * Interface describing an EventHandler. An EventHandler operates on it's owner when an Event is received. By convention each EventHandler should only handle specific events and ignores others
 * Created by themetalone on 10.07.15.
 */
public interface EventHandler {

    void setOwner(Object owner);

    /**
     * Handles the incoming Event.
     * @param e Event
     */
    void handle(Event e);

}
