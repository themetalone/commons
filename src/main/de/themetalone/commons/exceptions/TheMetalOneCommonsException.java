package de.themetalone.commons.exceptions;

/**
 * The top level {@link Exception} of the {@link de.themetalone.commons} library
 * Created by Steffen on 15.10.2015.
 */
public class TheMetalOneCommonsException extends Exception {

    public TheMetalOneCommonsException(){}
    public TheMetalOneCommonsException(String message){
        super(message);
    }

}
