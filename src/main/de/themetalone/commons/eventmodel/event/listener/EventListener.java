package de.themetalone.commons.eventmodel.event.listener;

import de.themetalone.commons.eventmodel.event.Event;
import de.themetalone.commons.eventmodel.event.handler.EventHandler;

/**
 * @author themetalone on 10.07.15.
 */
public interface EventListener {

    /**
     * listen to an event
     * @param e event to be listened to
     */
    void listenTo(Event e);

    /**
     * fire an event into the channel
     * @param e event to be published
     */
    void fire(Event e);

    /**
     * Adds an EventHandler
     * @param handler the EventHandler to be added
     */
    void addHandler(EventHandler handler);

}
