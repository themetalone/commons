package de.themetalone.commons.eventmodel.channel;


import de.themetalone.commons.eventmodel.event.Event;
import de.themetalone.commons.eventmodel.event.listener.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by themetalone on 10.07.15.
 */
public class ChannelImpl implements Channel {

    private static Logger LOG = LoggerFactory.getLogger(Channel.class);

    private String name;
    private List<EventListener> listeners;

    //TODO
    ChannelImpl(String name) {
        this.name=name;
        listeners = new LinkedList<>();
    }

    @Override
    public void register(EventListener eventListener) {
        LOG.debug(eventListener.toString()+" registered at "+name);
        for(EventListener listener:listeners){
            if(eventListener.equals(listener)) return;
        }
        listeners.add(eventListener);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void fire(Event e) {
        LOG.info(name+":"+e.toString());
        for(EventListener listener:listeners){
            (new EventThread(listener,e)).run();
        }
    }

    class EventThread extends Thread{

        private final EventListener listener;
        private final Event event;

        public EventThread(EventListener listener, Event event){
            this.listener=listener;
            this.event=event;
        }

        @Override
        public void run(){
            listener.listenTo(event);
        }
    }

}
