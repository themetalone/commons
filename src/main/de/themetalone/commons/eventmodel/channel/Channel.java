package de.themetalone.commons.eventmodel.channel;

import de.themetalone.commons.eventmodel.event.Event;
import de.themetalone.commons.eventmodel.event.listener.EventListener;

/**
 * Created by themetalone on 10.07.15.
 */
public interface Channel {

    void register(EventListener eventListener);

    String getName();

    void fire(Event e);

}
