package de.themetalone.commons.eventmodel.channel;



import java.util.LinkedList;
import java.util.List;

/**
 * Created by themetalone on 10.07.15.
 */
public class ChannelFactory implements IChannelFactory {

    static private IChannelFactory instance = new ChannelFactory();
    private List<Channel> channels = new LinkedList<>();

    private ChannelFactory(){};

    static private IChannelFactory getInstance(){return instance;}
    static public void setInstance(IChannelFactory factory){instance=factory;}

    @Override
    public Channel getChannelForName(String name) {
        for(Channel c : channels){
            if(c.getName().equals(name)) {
                return c;
            }
        }
        Channel newChannel = new ChannelImpl(name);
        channels.add(newChannel);
        return newChannel;
    }
}
