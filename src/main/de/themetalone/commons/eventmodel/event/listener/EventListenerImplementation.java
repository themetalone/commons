package de.themetalone.commons.eventmodel.event.listener;

import de.themetalone.commons.eventmodel.event.Event;
import de.themetalone.commons.eventmodel.event.handler.EventHandler;
import de.themetalone.commons.eventmodel.channel.Channel;

import java.util.LinkedList;
import java.util.List;

/**
 * @author themetalone on 10.07.15.
 */
public abstract class EventListenerImplementation implements EventListener {

    private Channel channel;

    private final List<EventHandler> eventHandlerList;

    public EventListenerImplementation(Channel c){
        channel = c;
        c.register(this);
        eventHandlerList = new LinkedList<>();
    }

    @Override
    public synchronized void listenTo(Event e) {
        for(EventHandler handler:eventHandlerList){
            handler.handle(e);
        }
    }

    @Override
    public void fire(Event e) {
        channel.fire(e);
    }

    @Override
    public void addHandler(EventHandler handler) {
        eventHandlerList.add(handler);
        handler.setOwner(this);
    }
}
